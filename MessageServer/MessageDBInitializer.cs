﻿using System.Data.Entity;
using MessageServer.Entity;

namespace MessageServer
{
    class MessageDBInitializer : DropCreateDatabaseIfModelChanges<MessageDbContext>
    {
        protected override void Seed(MessageDbContext context)
        {
            User serg = new User {Username = "serg", Password = "12345"};
            User vasa = new User {Username = "vasa", Password = "12345"};

            context.Users.Add(serg);
            context.Users.Add(vasa);
            context.SaveChanges();
        }
    }
}
