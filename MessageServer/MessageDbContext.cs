﻿using System;
using System.Data.Entity;
using MessageServer.Entity;

namespace MessageServer
{
    class MessageDbContext : DbContext
    {
        public MessageDbContext() : base("DefaultConnection")
        {
            Database.SetInitializer(new MessageDBInitializer());
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Message> Messages { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Message>()
                .HasRequired(a => a.Sender)
                .WithMany(m => m.SentMessages)
                .HasForeignKey(u => u.SenderId).WillCascadeOnDelete(false);

            modelBuilder.Entity<Message>()
                .HasRequired(a => a.Recipient)
                .WithMany(m => m.RecievedMessages)
                .HasForeignKey(u => u.RecipientId).WillCascadeOnDelete(false);
        }
    }
}
