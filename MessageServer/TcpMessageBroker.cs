﻿using MessageServer.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace MessageServer
{
    class TcpMessageBroker
    {
        private TcpListener _tcpListener;
        private string lineEnd = Environment.NewLine;

        public delegate void OnErrorMessage(string message);
        public delegate void OnCmdMessage(string message);

        public event OnErrorMessage ErrorMessage;
        public event OnCmdMessage CmdMessage;

        public TcpMessageBroker(string ipAddrStr, int port)
        {
            try
            {
                _tcpListener = new TcpListener(IPAddress.Parse(ipAddrStr), port);
                _tcpListener.Start(10);
                _tcpListener.BeginAcceptTcpClient(MessageProcessingCalback, _tcpListener);
            }
            catch (SocketException e)
            {
                ErrorMessage?.Invoke($"ERROR: {e.Message}");
            }
        }

        private void MessageProcessingCalback(IAsyncResult ar)
        {
            User loggedIn = null;
            bool running = true;
            TcpListener l = ar.AsyncState as TcpListener;
            TcpClient client = l.EndAcceptTcpClient(ar);
            l.BeginAcceptTcpClient(MessageProcessingCalback, l);

            StreamReader reader = new StreamReader(client.GetStream());
            StreamWriter writer = new StreamWriter(client.GetStream());
            while (running)
            {
                if (TryParseComand(reader.ReadLine(), out string cmd, out string[] cmdParams))
                {
                    writer.Write(ProcessCommand(cmd, ref running , ref loggedIn, cmdParams));
                }
                else
                {
                    ErrorMessage?.Invoke("ERROR: Command line wasn't recognized!");
                    writer.WriteLine("CMD_ERROR");
                }
                writer.Flush();
            }
            client.Close();
        }

        private bool TryParseComand(string cmdLine, out string cmd, out string[] cmdParams)
        {
            var l = cmdLine.Split(':');
            cmd = l[0];
            cmdParams = new[] { "" };
            if (String.IsNullOrEmpty(cmd)) return false;
            if (l.Length > 1)
                cmdParams = l[1].Split(';');
            return true;
        }

        private string ProcessCommand(string cmd, ref bool running , ref User loggedIn, string[] cmdParams)
        {
            switch (cmd)
            {
                case "LOGIN":
                    if (loggedIn != null)
                    {
                        return $"LOGGED_ALREADY{lineEnd}";
                    }
                    else
                    {
                        if (cmdParams.Length == 2)
                        {
                            using (MessageDbContext ctx = new MessageDbContext())
                            {
                                string username = cmdParams[0];
                                User user = ctx.Users.SingleOrDefault(u => u.Username == username);
                                if (user != null && user.Password == cmdParams[1])
                                {
                                    loggedIn = user;
                                    return $"LOGGEDIN{lineEnd}";
                                }
                            }
                        }
                    }
                    return $"NOTLOGGED{lineEnd}";
                case "LOGOUT":
                    if (loggedIn != null)
                    {
                        loggedIn = null;
                        running = false;
                        return $"LOGGEDOUT{lineEnd}";
                    }
                    return $"NOTLOGGED{lineEnd}";
                case "SEND_TO":
                    if (loggedIn != null)
                    {
                        if (cmdParams.Length == 2)
                        {
                            using (MessageDbContext ctx = new MessageDbContext())
                            {
                                string recipient = cmdParams[0];
                                string message = cmdParams[1];
                                User recipientUser = ctx.Users.SingleOrDefault(u => u.Username == recipient);
                                if (recipientUser != null)
                                {
                                    ctx.Messages.Add(new Message()
                                    {
                                        RecipientId = recipientUser.Id,
                                        SenderId = loggedIn.Id,
                                        Content = message
                                    });
                                    if(ctx.SaveChanges() != 1)
                                        return $"SEND_TO_ERR{lineEnd}";
                                }

                                return $"MSG_SENT{lineEnd}";
                            }
                        }
                    }
                    return $"NOTLOGGED{lineEnd}";
                case "GET_ALL":
                    if (loggedIn != null)
                    {
                        using (MessageDbContext ctx = new MessageDbContext())
                        {
                            long userId = loggedIn.Id;
                            StringBuilder retStr = new StringBuilder();
                            List<Message> messageslList = ctx.Messages.Where(m => m.RecipientId == userId).ToList();
                            foreach (Message message in messageslList)
                            {
                                User sender = ctx.Users.Find(message.SenderId);
                                retStr.AppendLine($"ID:{message.Id} From:{sender.Username} Sent:{message.CreateTime.ToString("yy-MM-dd HH:mm")} | {message.Content.Truncate(30)}");
                            }

                            return retStr.ToString();
                        }
                    }
                    return $"NOTLOGGED{lineEnd}";
                case "GET_UNREAD":
                    if (loggedIn != null)
                    {
                        using (MessageDbContext ctx = new MessageDbContext())
                        {
                            long userId = loggedIn.Id;
                            StringBuilder retStr = new StringBuilder();
                            List<Message> messageslList = ctx.Messages.Where(m => m.ReadTime == null).Where(m => m.RecipientId == userId).ToList();
                            foreach (Message message in messageslList)
                            {
                                User sender = ctx.Users.Find(message.SenderId);
                                retStr.AppendLine($"ID:{message.Id} From:{sender.Username} Sent:{message.CreateTime.ToString("yy-MM-dd HH:mm")} | {message.Content.Truncate(15)}");
                            }

                            return retStr.ToString();
                        }
                    }
                    return $"NOTLOGGED{lineEnd}";
                case "READ_MSG":
                    if (loggedIn != null)
                    {
                        if (cmdParams.Length == 1)
                        {
                            using (MessageDbContext ctx = new MessageDbContext())
                            {
                                long msgId = Int64.Parse(cmdParams[0]);
                                long userId = loggedIn.Id;
                                Message message = ctx.Messages.Where(m => m.Id == msgId).SingleOrDefault(m => m.RecipientId == userId);

                                if (message != null)
                                {
                                    User sender = ctx.Users.Find(message.SenderId);
                                    message.ReadTime = DateTime.Now;
                                    ctx.SaveChanges();
                                    return $"ID:{message.Id} From:{sender.Username} Sentdate:{message.CreateTime.ToString()}{lineEnd}{message.Content}{lineEnd}";
                                }

                                return $"MSG_NOT_FOUND{lineEnd}";
                            }
                        }
                    }
                    return $"NOTLOGGED{lineEnd}";
                default:
                    return $"CMD_ERROR{lineEnd}";

            }
        }

    }
}
