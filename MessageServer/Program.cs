using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageServer
{
    class Program
    {

        static void Main(string[] args)
        {
            TcpMessageBroker tmb = new TcpMessageBroker("127.0.0.1", 65535);
            tmb.ErrorMessage += Tmb_ErrorMessage;
            Console.ReadKey();
        }

        private static void Tmb_ErrorMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}
