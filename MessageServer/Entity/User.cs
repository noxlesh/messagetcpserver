﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageServer.Entity
{
    class User
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        //[InverseProperty("Sender")]
        public virtual ICollection<Message> SentMessages { get; set; }

        //[InverseProperty("Recipient")]
        public virtual ICollection<Message> RecievedMessages { get; set; }
    }
}
