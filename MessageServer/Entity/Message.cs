﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageServer.Entity
{
    class Message
    {
        public Message()
        {
            CreateTime = DateTime.Now;
        }

        public long Id { get; set; }
        public string Content { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? ReadTime { get; set; }
        public long SenderId { get; set; }
        public long RecipientId { get; set; }

        //[ForeignKey("SenderId")]
        public virtual User Sender { get; set; }
        //[ForeignKey("RecipientId")]
        public virtual  User Recipient { get; set; }

    }
}
